const { start } = require('./ipfs-gateway/ipfs-gateway');

const startServer = async ({ host, port }) => {
  console.warn('startServer::starting');

  let startResult;

  try {
    startResult = await start(host, port);
  } catch (err) {
    console.error(err);
    startResult = err;
  }
  if (startResult) {
    console.warn('startServer::starting::failed');
    return startResult;
  }
  console.warn('startServer::starting::success');
};

module.exports = {
  start: startServer,
};
