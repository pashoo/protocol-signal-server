const configurationNodeJs = require('ipfs/src/core/runtime/config-nodejs');

const IPFS_GATEWEAY_CONFIGURATION_STANDARD = configurationNodeJs;

const IPFS_GATEWEAY_CONFIGURATION_COMMON = {
  ...IPFS_GATEWEAY_CONFIGURATION_STANDARD,
};

module.exports = {
  IPFS_GATEWEAY_CONFIGURATION_COMMON,
};
