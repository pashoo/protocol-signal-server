const {
  IPFS_GATEWEAY_CONFIGURATION_HOST_DEFAULT,
  IPFS_GATEWEAY_CONFIGURATION_PORT_DEFAULT,
} = require('./ipfs-gateway-defaults.conf');
const {
  IPFS_GATEWEAY_CONFIGURATION_COMMON,
} = require('./ipfs-gateway-common.conf');

/**
 * @param {string} [IPFS_GATEWEAY_CONFIGURATION_HOST_DEFAULT] host - host to run the gateway on
 * @param {string} [IPFS_GATEWEAY_CONFIGURATION_PORT_DEFAULT] publicPort - public port number which is listen by the gateway server
 */
const getIpfsGatewayConf = (
  host = IPFS_GATEWEAY_CONFIGURATION_HOST_DEFAULT,
  publicPort = IPFS_GATEWEAY_CONFIGURATION_PORT_DEFAULT
) => ({
  repoOwner: true,
  init: true,
  start: true,
  silent: false,
  EXPERIMENTAL: {
    pubsub: true,
    ipnsPubsub: true,
    sharding: true,
    dht: true,
  },
  config: {
    Addresses: {
      ...IPFS_GATEWEAY_CONFIGURATION_COMMON.Addresses,
      Swarm: [`/ip4/${host}/tcp/4002`, `/ip4/${host}/tcp/4003/ws`],
      API: `/ip4/${host}/tcp/5002`,
      Gateway: `/ip4/${host}/tcp/${publicPort}`,
      Delegates: [],
    },
    Bootstrap: [],
    Pubsub: {
      Router: 'gossipsub',
      Enabled: true,
    },
  },
});

module.exports = {
  getIpfsGatewayConf,
};
