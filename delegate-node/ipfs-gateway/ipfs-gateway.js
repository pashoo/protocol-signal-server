const Ipfs = require('ipfs');
const { getIpfsGatewayConf } = require('./ipfs-gateway.conf');
/**
 * start ipfs node used as a delegate node
 * @param {string} host - an ip address where the gateway server must listen the incoming requests
 * @param {string | number} publicPort - a port number open for a public connections
 * @returns {Promise<IPFS | Error>} - returns true or Error
 */
const startIpfsNode = async (host, publicPort) => {
  if (!host) {
    return new Error('A host address must be provided');
  }
  if (!publicPort) {
    return new Error(
      'A port number opened for incoming connections must be provided'
    );
  }

  const nodeConfiguration = getIpfsGatewayConf(host, publicPort);

  if (!nodeConfiguration) {
    return new Error('Failed to resolve the node configuration');
  }

  console.warn('startIpfsNode::create');
  let node;

  try {
    node = await Ipfs.create(nodeConfiguration);
  } catch (err) {
    console.warn('startIpfsNode::create::failed');
    return err;
  }
  console.warn('startIpfsNode::waiting for ready');
  return node;
};

/**
 * run gateway on host and port provided
 * @param {IPFS} node - the ipfs node started before
 * @returns {Promise<Error | undefined>}
 */
const startGatewayServer = async node => {
  const Gateway = require('ipfs/src/http');
  const gateway = new Gateway(node);

  try {
    console.warn('runGatewayServer::start');
    await gateway.start();
    console.warn('runGatewayServer::start::success');
  } catch (err) {
    console.warn('runGatewayServer::start::error');
    return err;
  }
};

/**
 * start ipfs node and a gateway
 * @param {string} host - an ip address where the gateway server must listen the incoming requests
 * @param {string | number} publicPort - a port number open for a public connections
 * @returns {Promise<undefined | Error>} - returns true or Error
 */
const start = async (host, publicPort) => {
  const ipfsNode = await startIpfsNode(host, publicPort);

  if (ipfsNode instanceof Error) {
    console.error(ipfsNode);
    console.error('Failed to start the ipfs node');
    return ipfsNode;
  }
  const nodeId = await ipfsNode.id();

  console.warn(`The nodeId is ${nodeId.id}`);

  const gatewayStartResult = await startGatewayServer(ipfsNode);

  if (gatewayStartResult instanceof Error) {
    console.error(gatewayStartResult);
    console.error('Failed to start the gateway server');
    return gatewayStartResult;
  }
  console.warn(`The ipfs gateway server will listen on ${host}:${publicPort}`);
};

module.exports = {
  start,
};
