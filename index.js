const WebrtcStarSignal = require('./webrtc-star-signal-server');
const WebSocketStarSignal = require('./websocket-star-signal-server');
const DelegateNode = require('./delegate-node');
const SignalProxy = require('./signal-server-proxy');
const configuration = require('./conf');

/* *
 * To use a path on a multiaddress declared in the Swarm -> Address
 * into ipfs node start configuration use the example from
 * the https://github.com/multiformats/multiaddr:
 * "
 *  Encapsulation based on context
 *  Although multiaddrs are self-describing, it's possible to further encapsulate them based on context. For example in a web browser, it's obvious that, given a hostname, HTTP should be spoken. The specifics of this HTTP connection are not important (expect maybe the use of TLS), and will be derived from the browser's capabilities and configuration.
 *
 *  example.com/index.html
 *  /http/example.com/index.html
 *  /tls/sni/example.com/http/example.com/index.html
 *  /dns4/example.com/tcp/443/tls/sni/example.com/http/example.com/index.html
 *  /ip4/1.2.3.4/tcp/443/tls/sni/example.com/http/example.com/index.html
 * "
 * */

const {
  SIGNAL_PROXY_DELEGATE_NODE_HOST,
  SIGNAL_PROXY_DELEGATE_NODE_PORT,
  SIGNAL_PROXY_DELEGATE_NODE_API_PORT,
} = configuration;
const delegateNodeConf = {
  port: SIGNAL_PROXY_DELEGATE_NODE_PORT,
  host: SIGNAL_PROXY_DELEGATE_NODE_HOST,
};

async function startSignalServers() {
  try {
    const [
      wrtcStarSignalRunningOnConf,
      wsStarSignalRunningOnConf,
      delegateNodeStartResult,
    ] = await Promise.all([
      WebrtcStarSignal.start(),
      WebSocketStarSignal.start(),
      DelegateNode.start({
        ...delegateNodeConf,
      }),
    ]);

    if (delegateNodeStartResult instanceof Error) {
      console.error('Failed to start the delegate node');
      return delegateNodeStartResult;
    }
    if (wrtcStarSignalRunningOnConf instanceof Error) {
      console.error('Failed to start WebRTC star singal');
      return wrtcStarSignalRunningOnConf;
    }
    if (wsStarSignalRunningOnConf instanceof Error) {
      console.error('Failed to start WebSocket star rendevu');
      return wsStarSignalRunningOnConf;
    }
    return {
      wrtcStarSignalRunningOnConf,
      wsStarSignalRunningOnConf,
    };
  } catch (err) {
    return err;
  }
}

async function startProxyServer(options, runningOnConfSignalServers) {
  const {
    wrtcStarSignalRunningOnConf,
    wsStarSignalRunningOnConf,
  } = runningOnConfSignalServers;
  const {
    host,
    port,
    wrtcStarSignalServerPath,
    wsStarSignalServerPath,
  } = options;

  try {
    await SignalProxy.start({
      proxy: { host, port },
      wrtcStarSignal: {
        ...wrtcStarSignalRunningOnConf,
        path: wrtcStarSignalServerPath,
      },
      websocketStarSignal: {
        ...wsStarSignalRunningOnConf,
        path: wsStarSignalServerPath,
      },
      delegateNode: {
        host: SIGNAL_PROXY_DELEGATE_NODE_HOST,
        port: SIGNAL_PROXY_DELEGATE_NODE_API_PORT,
      },
    });

    return true;
  } catch (err) {
    return err;
  }
}

async function start() {
  const {
    SIGNAL_PROXY_SERVER_HOST,
    SIGNAL_PROXY_SERVER_PORT,
    SIGNAL_PROXY_SERVER_WS_SIGNAL_SERVER_PATH,
    SIGNAL_PROXY_SERVER_WRTC_SIGNAL_SERVER_PATH,
  } = configuration;
  let runningOnSignalServerConfigurations;

  try {
    runningOnSignalServerConfigurations = await startSignalServers();
  } catch (err) {
    runningOnSignalServerConfigurations = err;
  }
  if (runningOnSignalServerConfigurations instanceof Error) {
    console.error('Failed to start one of the signal servers');
    console.error(runningOnSignalServerConfigurations);
    process.exit(1);
  }

  try {
    await startProxyServer(
      {
        host: SIGNAL_PROXY_SERVER_HOST,
        port: SIGNAL_PROXY_SERVER_PORT,
        wrtcStarSignalServerPath: SIGNAL_PROXY_SERVER_WRTC_SIGNAL_SERVER_PATH,
        wsStarSignalServerPath: SIGNAL_PROXY_SERVER_WS_SIGNAL_SERVER_PATH,
      },
      runningOnSignalServerConfigurations
    );
  } catch (err) {
    console.error('Failed to start the proxy server');
    console.error(err);
    process.exit(1);
  }
  console.log(
    `The server is running on ${SIGNAL_PROXY_SERVER_HOST}:${SIGNAL_PROXY_SERVER_PORT}
     The WebSocket signal star rendevu server request must be use the path: ${SIGNAL_PROXY_SERVER_WS_SIGNAL_SERVER_PATH}
     The WebRTC signal star signal server request must be use the path: ${SIGNAL_PROXY_SERVER_WRTC_SIGNAL_SERVER_PATH}
    `
  );
  return true;
}

start();
