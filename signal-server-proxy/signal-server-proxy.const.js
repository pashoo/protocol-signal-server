const SIGNAL_PROXY_SERVER_OPTIONS_COMMON = {
  xfwd: false,
  ws: true,
  autoRewrite: false,
};

module.exports = {
  SIGNAL_PROXY_SERVER_OPTIONS_COMMON,
};
