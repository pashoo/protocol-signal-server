const httpProxy = require('http-proxy');
const http = require('http');
const url = require('url');
const signalProxyOptions = require('./signal-server-proxy.const');

/**
 * HttpProxyOptions
 *  {
 *    target : <url string to be parsed with the url module>
 *    forward: <url string to be parsed with the url module>
 *    agent  : <object to be passed to http(s).request>
 *    ssl    : <object to be passed to https.createServer()>
 *    ws     : <true/false, if you want to proxy websockets>
 *    xfwd   : <true/false, adds x-forward headers>
 *    secure : <true/false, verify SSL certificate>
 *    toProxy: <true/false, explicitly specify if we are proxying to another proxy>
 *    prependPath: <true/false, Default: true - specify whether you want to prepend the target's path to the proxy path>
 *    ignorePath: <true/false, Default: false - specify whether you want to ignore the proxy path of the incoming request>
 *    localAddress : <Local interface string to bind for outgoing connections>
 *    changeOrigin: <true/false, Default: false - changes the origin of the host header to the target URL>
 *    preserveHeaderKeyCase: <true/false, Default: false - specify whether you want to keep letter case of response header key >
 *    auth   : Basic authentication i.e. 'user:password' to compute an Authorization header.
 *    hostRewrite: rewrites the location hostname on (201/301/302/307/308) redirects, Default: null.
 *    autoRewrite: rewrites the location host/port on (201/301/302/307/308) redirects based on requested host/port. Default: false.
 *    protocolRewrite: rewrites the location protocol on (201/301/302/307/308) redirects to 'http' or 'https'. Default: null.
 */

/**
 *
 * proxy queries to a one enpoint to support both of
 * websocket star rendevu server and webrtc star signal
 * server
 * @param {HttpProxyOptions} proxyServerOptions
 * @param {object} options
 * @param {object} options.proxy - options for proxy server
 * @param {string} options.proxy.port - port where to listen requests before proxy it
 * @param {string} options.proxy.host - host where to listen requests before proxy it
 * @param {object} options.delegateNode - options for proxying requests to the WebSocket signal server
 * @param {object} options.delegateNode.port - port where the Delegate router is running
 * @param {object} options.delegateNode.host - host where the Delegate router is running
 * @param {object} options.wrtcStarSignal - options for proxying requests to the WebRTCStar signal server
 * @param {object} options.wrtcStarSignal.port - port where the WebRTC star signal server is running
 * @param {object} options.wrtcStarSignal.host - host where the WebRTC star signal server is running
 * @param {object} options.wrtcStarSignal.path - if a request path equals to it, then the request will be proxied to the WebRTC star signal server
 * @param {object} options.websocketStarSignal - options for proxying requests to the WebSocket signal server
 * @param {object} options.websocketStarSignal.port - port where the WebSocket star signal server is running
 * @param {object} options.websocketStarSignal.host - host where the WebSocket star signal server is running
 * @param {object} options.websocketStarSignal.path - if a request path equals to it, then the request will be proxied to the WebSocket star signal server
 */
async function start(options, proxyServerOptions) {
  const { SIGNAL_PROXY_SERVER_OPTIONS_COMMON } = signalProxyOptions;
  const { proxy, wrtcStarSignal, websocketStarSignal, delegateNode } = options;
  const { host: mainHost, port: mainPort } = proxy;
  const { host: delegateNodeHost, port: delegateNodePort } = delegateNode;
  const {
    host: wrtStarSignalHost,
    port: wrtStarSignalPort,
    path: wrtStarSignalPath,
  } = wrtcStarSignal;
  const wrtcStarSignalPathResolved = wrtStarSignalPath;
  const {
    host: wsStarSignalHost,
    port: wsStarSignalPort,
    path: wsStarSignalPath,
  } = websocketStarSignal;
  const wsStarSignalPathResolved = wsStarSignalPath;
  const proxyDelegateNode = httpProxy.createProxyServer({
    ...proxyServerOptions,
    ...SIGNAL_PROXY_SERVER_OPTIONS_COMMON,
    target: {
      host: delegateNodeHost,
      port: delegateNodePort,
    },
  });
  const proxyWSStarSignal = httpProxy.createProxyServer({
    ...proxyServerOptions,
    ...SIGNAL_PROXY_SERVER_OPTIONS_COMMON,
    target: {
      host: wsStarSignalHost,
      port: wsStarSignalPort,
    },
    ws: true,
  });
  const proxyWebRTCStarSignal = httpProxy.createProxyServer({
    ...proxyServerOptions,
    ...SIGNAL_PROXY_SERVER_OPTIONS_COMMON,
    target: {
      host: wrtStarSignalHost,
      port: wrtStarSignalPort,
    },
    ws: true,
  });
  const httpServer = http.createServer(function(req, res) {
    try {
      const urlParsed = url.parse(req.url);
      const { path } = urlParsed;

      if (path.startsWith(wrtcStarSignalPathResolved)) {
        proxyWebRTCStarSignal.web(req, res);
      } else if (path.startsWith(wsStarSignalPathResolved)) {
        proxyWSStarSignal.web(req, res);
      } else {
        proxyDelegateNode.web(req, res);
      }
    } catch (err) {
      console.error('Failed to serve the request');
      console.error(err);
    }
  });

  httpServer.on('upgrade', function(req, socket, head) {
    try {
      const urlParsed = url.parse(req.url);
      const { path } = urlParsed;

      if (path.includes(wrtcStarSignalPathResolved)) {
        proxyWebRTCStarSignal.ws(req, socket, head);
      } else if (path.includes(wsStarSignalPathResolved)) {
        proxyWSStarSignal.ws(req, socket, head);
      } else {
        proxyDelegateNode.ws(req, res, head);
      }
    } catch (err) {
      console.error('Failed to serve the request');
      console.error(err);
    }
  });
  httpServer.listen(mainPort, mainHost);
}

module.exports = {
  start,
};
