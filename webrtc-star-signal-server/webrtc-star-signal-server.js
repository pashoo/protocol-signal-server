const wrtc = require('wrtc');
const WRTCStar = require('libp2p-webrtc-star');
const SignalServer = require('libp2p-webrtc-star/src/sig-server');
const conf = require('./webrtc-star-signal-server.conf');

/**
 * start signal server to use it with webrtc star
 * * e.g. fot the js-ipfs instannce
 * Addresses: {
 *  Swarm: ['/dns4/wrtc-star.discovery.libp2p.io/tcp/443/wss/p2p-webrtc-star']
 * }
 * @returns {object | Error} - return options { host, port }, where it's running on or an error if failed to start
 */
async function start() {
  const { WEBRTC_STAR_SIGNAL_SERVER_OPTIONS } = conf;

  try {
    await SignalServer.start(WEBRTC_STAR_SIGNAL_SERVER_OPTIONS);
    console.log(
      `Signal server was running on ${WEBRTC_STAR_SIGNAL_SERVER_OPTIONS.host}:${WEBRTC_STAR_SIGNAL_SERVER_OPTIONS.port}`
    );
  } catch (err) {
    console.error('Failed to start webrtc star signal server');
    console.error(err);
    return err;
  }
  return WEBRTC_STAR_SIGNAL_SERVER_OPTIONS;
}

module.exports = {
  start,
};
