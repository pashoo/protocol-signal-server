const WebSocketStarRendevu = require('libp2p-websocket-star-rendezvous');
const WebSocketStarSignalServerConf = require('./websocket-star-signal-server.conf');

/**
 * start signal server to use it with websocket star
 * e.g. fot the js-ipfs instannce
 * Addresses: {
 *  Swarm: ['/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star']
 * }
 * @returns {object | Error} - return options { host, port }, where it's running on or an error if failed to start
 */
async function start() {
  const {
    WEBSOCKET_STAR_SIGNAL_SERVER_OPTIONS,
  } = WebSocketStarSignalServerConf;

  try {
    await WebSocketStarRendevu.start(WEBSOCKET_STAR_SIGNAL_SERVER_OPTIONS);
    console.log(
      `WebSocket star rendevu server started at ${WEBSOCKET_STAR_SIGNAL_SERVER_OPTIONS.host}:${WEBSOCKET_STAR_SIGNAL_SERVER_OPTIONS.port}`
    );
  } catch (err) {
    console.error('Failed to start the WebSocket start rendevu server');
    console.error(err);
    return err;
  }
  return WEBSOCKET_STAR_SIGNAL_SERVER_OPTIONS;
}

module.exports = {
  start,
};
